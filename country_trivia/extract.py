import json

capitals = []

with open('country_list.json') as data_file:
    countries = json.load(data_file)
for country in countries:
    if country.get('capital'):
        capitals.append(country.get('capital'))
    capitals += country.get('capital_alternates')

# with open('data/countriesToCities.json') as data_file:
#     countries_to_cities = json.load(data_file)
# for item in countries_to_cities:
#     capitals += countries_to_cities[item]

capitals = list(set(capitals))
capitals.sort()

open('customSlotTypes/LIST_OF_CITIES', 'w').close()
with open('customSlotTypes/LIST_OF_CITIES', 'a') as file:
    for index, capital in enumerate(capitals):
        file.write('{}{}'.format(capital.encode('utf-8').strip(), '\n' if index < len(capitals) - 1 else ''))
