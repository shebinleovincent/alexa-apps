import logging
import os
import json
import random
from flask_ask import Ask, request as ask_request, session, question, statement
from datetime import datetime

logging.getLogger('flask_ask').setLevel(logging.DEBUG)

CARD_TITLE = 'Country Trivia'

SESSION_COUNTRY = 'country'
SESSION_PICKED = 'picked'
SESSION_SCORE = 'score'
SESSION_QUESTION_COUNT = 'question_count'

OOPS = ['Oops!', 'Ouch!', 'Sorry!', 'Err!']
THAT_IS_WRONG = ['That is wrong.', 'That is not correct.', 'That is incorrect.', 'That is not right.']
YOU_ARE_CORRECT = [
    'You are correct.', 'You are absolutely correct!', 'That is exactly correct!', 'That is correct.',
    'You are right.', 'You are absolutely correct!', 'That is exactly right!', 'That is right.',
    'There you go.', 'Right on!', 'Exactly!'
]
YOUR_NEXT_QUESTION = ['Your next question.', 'Here is your next question.', 'Next one.']

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(os.path.join(__location__, 'data/country_list.json')) as data_file:
    countries = json.load(data_file)

# create match array for country name and capitals
for cty in countries:
    cty['name_match'] = ([cty.get('name').lower()] if cty.get('name') else []) + [name.lower() for name in cty.get('name_alternates')]
    cty['capital_match'] = ([cty.get('capital').lower()] if cty.get('capital') else []) + [name.lower() for name in cty.get('capital_alternates')]


def init(app):
    ask = Ask(app, '/')
    
    # Session starter
    #
    # This intent is fired automatically at the point of launch (= when the session starts).
    # Use it to register a state machine for things you want to keep track of, such as what the last intent was, so as to be
    # able to give contextual help.
    @ask.on_session_started
    def start_session():
        """
        Fired at the start of the session, this is a great place to initialise state variables and the like.
        """
        logging.debug('Session started at {}'.format(datetime.now().isoformat()))
    
    # Launch intent
    #
    # This intent is fired automatically at the point of launch.
    # Use it as a way to introduce your Skill and say hello to the user. If you envisage your Skill to work using the
    # one-shot paradigm (i.e. the invocation statement contains all the parameters that are required for returning the
    # result
    @ask.launch
    def handle_launch():
        """
        (QUESTION) Responds to the launch of the Skill with a welcome statement and a card.

        * Initial statement: welcome_text
        * Reprompt statement: welcome_re_text
        * Card title: CARD_TITLE
        * Card body: welcome_card_text
        """
        
        welcome_text = 'Welcome to {}, Are you ready to explore capitals and countries?'.format(CARD_TITLE)
        welcome_re_text = 'Shall we start?'
        welcome_card_text = welcome_text
        return question(welcome_text).reprompt(welcome_re_text).simple_card(CARD_TITLE, welcome_card_text)
    
    @ask.intent('AMAZON.HelpIntent')
    def handle_help():
        """
        (QUESTION) Handles the 'help' built-in intention.

        You can provide context-specific help here by rendering templates conditional on the help referrer.
        """
        
        help_text = '{} will help you learn about capitals of countries! Are you ready?'.format(CARD_TITLE)
        help_re_text = 'Shall we start?'
        return question(help_text).reprompt(help_re_text).simple_card(CARD_TITLE, help_text)
    
    @ask.intent('AMAZON.CancelIntent')
    @ask.intent('AMAZON.StopIntent')
    def handle_stop():
        """
        (STATEMENT) Handles the 'stop' built-in intention.
        """
        
        stop_text = ''
        if session.attributes.get(SESSION_QUESTION_COUNT) and session.attributes.get(SESSION_QUESTION_COUNT) > 1:
            stop_text += 'Your got {} correct out of {}. '.format(session.attributes.get(SESSION_SCORE),
                                                                  session.attributes.get(SESSION_QUESTION_COUNT) - 1)
        stop_text += 'Thank you for playing {}. Goodbye!'.format(CARD_TITLE)
        stop_card_text = stop_text
        return statement(stop_text).simple_card(CARD_TITLE, stop_card_text)
    
    @ask.intent('CountryTriviaIntent')
    def handle_country_trivia(capital):
        """
        (QUESTION) Handles the 'country trivia'  ask question about country like capital.
        """
        
        trivia_text = ''
        if session.attributes.get(SESSION_COUNTRY):
            country = session.attributes.get(SESSION_COUNTRY)
            if capital and capital.lower() in country.get('capital_match'):
                session.attributes[SESSION_SCORE] = session.attributes.get(SESSION_SCORE, 0) + 1
                trivia_text += random.choice(YOU_ARE_CORRECT)
            else:
                if capital:
                    trivia_text += random.choice(OOPS) + ' ' + random.choice(THAT_IS_WRONG) + ' '
                
                trivia_text += 'Capital of {} is {}.'.format(country.get('name'), country.get('capital'))
            
            trivia_text += ' ' + random.choice(YOUR_NEXT_QUESTION)
        
        country = find_country()
        session.attributes[SESSION_COUNTRY] = country
        session.attributes[SESSION_QUESTION_COUNT] = session.attributes.get(SESSION_QUESTION_COUNT, 0) + 1
        trivia_re_text = 'What is the capital of {}?'.format(country.get('name'))
        trivia_text = '{} {}'.format(trivia_text, trivia_re_text)
        trivia_card_text = trivia_text
        return question(trivia_text).reprompt(trivia_re_text).simple_card(CARD_TITLE, trivia_card_text)
    
    @ask.intent('CountryCapitalIntent')
    def handle_country_capital(country):
        """
        (QUESTION) Handles the 'country capital'  ask capital of a country.
        """
        
        if country:
            matched_country = find_country(country)
            if matched_country:
                capital = matched_country.get('capital')
                country_capital_text = 'Capital of {} is {}.'.format(country, capital)
            else:
                country_capital_text = 'Sorry, I couldn\'t find the capital of {}.'.format(country)
        else:
            country_capital_text = 'Sorry, I didn\'t quite understand that.'
        country_capital_re_text = 'Would you like to play {}?'.format(CARD_TITLE)
        country_capital_text = '{} {}'.format(country_capital_text, country_capital_re_text)
        country_capital_card_text = country_capital_text
        return question(country_capital_text).reprompt(country_capital_re_text).simple_card(CARD_TITLE,
                                                                                            country_capital_card_text)
    
    @ask.intent('AMAZON.StartOverIntent')
    def handle_start_over():
        """
        (QUESTION) Handles the 'start over!'  built-in intention.
        """
        
        session.attributes[SESSION_COUNTRY] = None
        session.attributes[SESSION_SCORE] = 0
        session.attributes[SESSION_QUESTION_COUNT] = 0
        return handle_country_trivia(None)
    
    @ask.intent('AMAZON.RepeatIntent')
    def handle_repeat():
        """
        (QUESTION) Handles the 'repeat'  built-in intention.
        """
        
        country = session.attributes.get(SESSION_COUNTRY)
        if country:
            repeat_text = 'What is the capital of {}?'.format(country.get('name'))
            repeat_re_text = repeat_text
            repeat_card_text = repeat_text
            return question(repeat_text).reprompt(repeat_re_text).simple_card(CARD_TITLE, repeat_card_text)
        else:
            return handle_country_trivia(None)
    
    # Ending session
    #
    # This intention ends the session.
    @ask.session_ended
    def session_ended():
        """
        Returns an empty for `session_ended`.

        .. warning::

        The status of this is somewhat controversial. The `official documentation`_ states that you cannot return a response
        to ``SessionEndedRequest``. However, if it only returns a ``200/OK``, the quit utterance (which is a default test
        utterance!) will return an error and the skill will not validate.

        """
        
        return statement("")


def find_country(country_name=None):
    matched_country = None
    if country_name:
        for country in countries:
            if country_name.lower() in country.get('name_match'):
                return country
    else:
        picked = session.attributes.get(SESSION_PICKED, [])
        
        while matched_country is None:
            random_country_index = random.randrange(0, len(countries))
            if countries[random_country_index].get('capital') and random_country_index not in picked:
                matched_country = countries[random_country_index]
                picked.append(random_country_index)
        
        session.attributes[SESSION_PICKED] = picked
    return matched_country
