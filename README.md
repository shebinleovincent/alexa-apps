# Alexa Country Trivia


## Requirements

  - Python = 3.5
  
  
## Installation and Usage


### Installation

https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-16-04


### Run from command line

```sh
$ pip install -r requirements.txt
$ python server.py
```


### Run tests

```sh
$ nosetests
```
